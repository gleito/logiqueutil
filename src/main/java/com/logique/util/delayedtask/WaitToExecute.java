/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.logique.util.delayedtask;

import java.util.Timer;

/**
 *
 * @author gustavoleitao
 */
public final class WaitToExecute {

    private Timer timer;
    private int miliseconds;
    private RemindTask task;
    public static final int DEFAULT_DELAY_TIME = 400; // 400 millisegundos

    public WaitToExecute(int miliseconds) {
        this.miliseconds = miliseconds;
    }

    private synchronized void startTimer(RemindTask task){
        this.task = task;
        timer = new Timer();
        timer.schedule(task, miliseconds);
    }

    public synchronized void restartTimer(RemindTask newtask){
        if (task != null){
            task.cancelTask();
        }
        startTimer(newtask);
    }

}
