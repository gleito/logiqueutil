/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.delayedtask;

import java.util.TimerTask;

/**
 *
 * @author gustavoleitao
 */
public abstract class RemindTask extends TimerTask {

    boolean canceled;

    public RemindTask() {
        canceled = false;
    }

    public void cancelTask() {
        canceled = true;
    }

    protected abstract void doTask();

    @Override
    public void run() {
        if (!canceled) {
            doTask();            
            cancel();
        } else {
        }
        
    }
}
