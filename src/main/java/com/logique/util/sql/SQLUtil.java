/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.sql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Classe com utilidades para a construção de comandos SQL.
 *
 * @author Gustavo Leitão
 */
public class SQLUtil {

    /**
     * A partir de uma lista de elementos selecionados e não selecionados ele
     * gera uma String de cláusula IN do SQL.
     *
     * @param column Coluna a ser utilizada para a construção da cláusula.
     * @param selectedItems Lista de itens selecionados na interface.
     * @param maxItems Número máximo de elementos por cláusula IN.
     * @return Cláusula IN do SQL.
     */
    public static String buildInSQL(String column, Collection<String> selectedItems, int maxItems) {
        return buildInSQL(column, selectedItems, null, maxItems);
    }

    /**
     * A partir de uma lista de elementos selecionados e não selecionados ele
     * gera uma String de cláusula IN do SQL.
     *
     * @param column Coluna a ser utilizada para a construção da cláusula.
     * @param selectedItems Lista de itens selecionados na interface.
     * @param notSelectedItems Lista de itens não selecionados.
     * @param maxItems Número máximo de elementos por cláusula IN.
     * @return Cláusula IN do SQL.
     */
    public static String buildInSQL(String column, Collection<String> selectedItems, Collection<String> notSelectedItems, int maxItems) {

        if (selectedItems == null || maxItems <= 0) {
            return "";
        }

        if (selectedItems.isEmpty()) {
            if (notSelectedItems != null && !notSelectedItems.isEmpty()) {
                return "1=0 ";
            }
            return "";
        }

        String operador1 = " or ";
        String operador2 = " in ";
        List<String> itens = new ArrayList<String>(selectedItems);

        if (notSelectedItems != null) {
            if (notSelectedItems.isEmpty()) {
                return " 1=1 ";
            } else {
                if (selectedItems.size() > notSelectedItems.size()) {
                    operador1 = " and ";
                    operador2 = " not in ";
                    itens = new ArrayList<String>(notSelectedItems);
                }
            }
        }

        StringBuilder filtro = new StringBuilder(" (");

        for (int i = 0; i <= (int) (itens.size() / maxItems); i++) {

            int inicio = i * maxItems;
            int fim = (i + 1) * maxItems;
            if (fim > itens.size()) {
                fim = itens.size();
            }

            List<String> itemList = itens.subList(inicio, fim);

            if (!itemList.isEmpty()) {
                if (i > 0) {
                    filtro.append(operador1);
                }

                filtro.append(column);
                filtro.append(operador2);
                filtro.append("(");

                filtro.append(toPlainSQLString(itemList));
                filtro.append(")");
            }



        }
        filtro.append(")");
        return filtro.toString();

    }

    private static String toPlainSQLString(List<String> items) {
        StringBuilder string = new StringBuilder();

        for (int i = 0; i < items.size(); i++) {
            String alarm = items.get(i).toString();
            string.append("'");
            string.append(alarm);
            string.append("'");
            if (i < items.size() - 1) {
                string.append(",");
            }
        }

        return string.toString().trim();
    }
}
