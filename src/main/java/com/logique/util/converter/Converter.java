/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.converter;

/**
 * Interface que converte dois tipos de dados
 * @author LS
 */
public interface Converter<T,V> {
    
    /**
     * Converte dois tipos de dados.
     * @param object Instancia de objeto a serconvertido.
     * @return Objeto convertido.
     */
    public V convert(T object);
    
}
