/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.controler;

import com.logique.util.controler.tasks.BackgroundTask;

/**
 * Classe de controle que executa uma determinada ação.
 *
 * @author Gustavo Leitão
 */
public class LogiqueControler {

    private static LogiqueControler INSTANCE = null;

    private LogiqueControler() {
    }

    public static synchronized LogiqueControler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LogiqueControler();
        }
        return INSTANCE;   
    }

    /**
     * Método responsável por executar uma determinada tarefa em background.
     *
     * @param task Tarefa a ser executada.
     */
    public synchronized void executeTask(BackgroundTask task) {
        task.execute();
    }
    
}
