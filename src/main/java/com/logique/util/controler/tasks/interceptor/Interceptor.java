/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.controler.tasks.interceptor;

/**
 *
 * @author Gustavo
 */
@Intercepts
public interface Interceptor {
    
    public void intercept();
    public boolean accepts();
    
}
