/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.controler.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingUtilities;

/**
 * Classe que realiza uma determinada ação em background.
 *
 * @author Gustavo Leitão
 */
public abstract class BackgroundTask<T, V> {

    private LogiqueSwingWorker<T, V> logiqueSwingWorker;
    private BackgroundTasktView<T, V> eventView;
    private List<V> partialResults;

    public BackgroundTask(BackgroundTasktView<T, V> eventView) {
        partialResults = new ArrayList<V>();
        this.eventView = eventView;
        logiqueSwingWorker = new LogiqueSwingWorker<T, V>(this);
    }

    /**
     * Método abstrato a ser implementado com a ação a ser executada em
     * background.
     *
     * @return O resultado da execução
     * @throws Exception Exceção lançada durante a execução da ação.
     */
    protected abstract T doInBackground() throws Exception;

    /**
     * Publica um resultado parcial
     *
     * @param resultList lista de resultados a serem publicados para
     * processamento na interface.
     */
    final protected void publish(V... resultList) {
        partialResults.addAll(Arrays.asList(resultList));
        processPartialResult();
    }

    /**
     * Método que retorna a lista de resultados publicados durante a aexecução
     * da ação através do método publish.
     *
     * @return
     */
    private List<V> getPartialResults() {
        return partialResults;
    }

    /**
     * Método que chama o processamento a ser realizado na interface durante o
     * processamento da tarefa passando a interface gráfica os resultados
     * parciais.
     */
    private void processPartialResult() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                eventView.process(getPartialResults());
            }
        });
    }

    /**
     * Método que chamará o processamento de interface gráfica a ser executado
     * após a execução da tarefa em background.
     */
    final void executeAfterEWTAction() {
        eventView.after();
    }

    /**
     * Método que chamará o processamento de interface gráfica a ser executado
     * antes da execução da tarefa em background.
     */
    final void executeBeforeEWTAction() {
        eventView.before();
    }

    /**
     * Atribui o resultado da ação da tarefa.
     *
     * @param result Resultado da ação.
     */
    final void setResult(T result) {
        eventView.setResult(result);
    }

    /**
     * Em caso de falha de execução seta a exceção ocorrida.
     *
     * @param exception Exceção ocorrida.
     */
    final void setResult(InterruptedException exception) {
        eventView.setResult(exception);
    }

    /**
     * Em caso de falha de execução seta a exceção ocorrida.
     *
     * @param exception Exceção ocorrida.
     */
    final void setResult(ExecutionException exception) {
        eventView.setResult(exception);
    }

    /**
     * Atribui o progresso da tarefa e chama a interface gráfica para dar
     * tratamento visual
     *
     * @param progress
     */
    protected final void setProgress(final int progress) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                eventView.setProgress(progress);
            }
        });
    }

    public final void execute() {        
        logiqueSwingWorker.execute();
    }
}
