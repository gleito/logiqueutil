/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.controler.tasks;

import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 * Classe responsável por executar as ações em background e gerenciar a execução
 * e respostas para interface gráfica swing
 *
 * @author Gustavo Leitão
 */
public final class LogiqueSwingWorker<T extends Object, V extends Object> extends SwingWorker<T, V> {

    private BackgroundTask<T, V> backgroundAction;

    /**
     * Construtor da classe.
     *
     * @param backgroundAction Uma implementação da interface BackgroundTask
     * onde terá a ação da tarefa implementada.
     */
    public LogiqueSwingWorker(BackgroundTask<T, V> backgroundAction) {
        setBackgroundAction(backgroundAction);
    }

    /**
     * Atribui um BackgroundTask a variável local.
     *
     * @param backgroundAction Implementação da interface BackgroundTask.
     */
    private void setBackgroundAction(BackgroundTask<T, V> backgroundAction) {
        this.backgroundAction = backgroundAction;
    }

    /**
     * Executa um determinada tarefa em background executando antes alguma ação
     * de interface
     *
     * @return O resulktado da ação.
     * @throws Exception Disparada caso alguma excessão ocorra durante a
     * execução da tarefa.
     */
    @Override
    protected final T doInBackground() throws Exception {
        executeBeforeOnEWT();
        return backgroundAction.doInBackground();
    }

    /**
     * Método chamado após a execução da tarefa.
     */
    @Override
    protected final void done() {

        try {
            backgroundAction.setResult(get());
        } catch (InterruptedException ex) {
            backgroundAction.setResult(ex);
        } catch (ExecutionException ex) {
            backgroundAction.setResult(ex);
        }

        backgroundAction.executeAfterEWTAction();

    }

    /**
     * Executa uma ação da interface antes de executar a tarefa em background
     */
    private void executeBeforeOnEWT() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                backgroundAction.executeBeforeEWTAction();
            }
        });
    }

    @Override
    protected final void process(List<V> list) {
    }
}
