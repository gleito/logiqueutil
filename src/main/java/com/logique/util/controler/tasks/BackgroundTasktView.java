/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.logique.util.controler.tasks;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Classe abstrata para a implementação de resultados a serem colocados na
 * interface swing para execução de ação de forma assíncrona.
 *
 * @author Gustavo Leitão
 */
public abstract class BackgroundTasktView<T, V> {

    private ExecutionException executionExeption = null;
    private InterruptedException interruptedException = null;
    private T result = null;
    private int progress;

    /**
     * Método de visibilidade default para ser repassado o resultado da execução
     * da tarefa.
     *
     * @param executionExeption Exceção lançada durante a execução da tarefa, se
     * houver.
     */
    final void setResult(ExecutionException executionExeption) {
        this.executionExeption = executionExeption;
    }

    /**
     * Método de visibilidade default para ser repassado o resultado da execução
     * da tarefa.
     *
     * @param interruptedException Exceção lançada durante a execução da tarefa,
     * se houver.
     */
    final void setResult(InterruptedException interruptedException) {
        this.interruptedException = interruptedException;
    }

    /**
     * Método de visibilidade default para ser repassado o resultado da execução
     * da tarefa.
     *
     * @param result Resultado da execução da tarefa.
     */
    final void setResult(T result) {
        this.result = result;
    }

    /**
     * Método responsável por retornar o resultado da tarefa.
     *
     * @return O resultado da execução da tarefa;
     * @throws ExecutionException Exceção que encapsula a exceção lançada
     * durante a execução da tarefa.
     * @throws InterruptedException Exceção lançada em caso de interrupção
     * abrupta da execução da tarefa.
     */
    protected final T getResult() throws ExecutionException, InterruptedException {

        if (executionExeption != null) {
            throw executionExeption;
        } else if (interruptedException != null) {
            throw executionExeption;
        }
        return result;

    }

    /**
     * Ação a ser executada antes de qualquer ação.
     */
    public void before(){}

    /**
     * Ação executada após uma BackgroundTask ser finalizada.
     */
    public abstract void after();

    /**
     * Processamento a ser realizado durante a execução da tarefa em backgrond.
     * Esse método é chamado a cada publicação de resultado pela tarefa.
     *
     * @param partialResults Lista de resultados parciais para processamento na
     * interface.
     */
    public void process(List<V> partialResults){}

    /**
     * 
     * @param progress 
     */
    protected final void setProgress(int progress) {
        this.progress = progress;
    }

    protected final int getProgress() {
        return progress;
    }
}
